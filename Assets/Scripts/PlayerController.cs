﻿using System.Collections;
using System;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static Action OnLifeChange;
    
    private static GameObject _instance = null;
 
    private enum ActionEnum
    {
        DashRight,
        DashLeft,
    }
    
    [SerializeField] private float _force;
    [SerializeField] private float _forceMove;
    [SerializeField] private Vector2 _velocity;
    [SerializeField] private float _timeToDoAction;
    
    [Header("Movement style")]
    [SerializeField] private bool _dash;
    [SerializeField] private bool _addForce;
    
    private Rigidbody2D _rigidbody;
    private int _selectedAction;
    
    public static int Life { get; private set; }
    public static GameObject Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = gameObject;
        }
        
        GameManager.OnResetGame += ResetLife;
        GameManager.OnTapButton += SelectAction;
        
        ResetLife();
    }

    private void OnEnable()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _velocity.y = 1;

        _selectedAction = (int)ActionEnum.DashLeft;
    
        StartCoroutine(DoAction());
    }

    private void OnDisable()
    {
        Destroy(gameObject);
    }

    private void SelectAction()
    {
        _selectedAction++;
        
        if (_selectedAction > 1)
        {
            _selectedAction = 0;
        }
    }

    public IEnumerator DoAction()
    {
        while (true)
        {
            switch (_selectedAction)
            {
                case (int)ActionEnum.DashRight:
                    Movement(Vector2.right);

                    break;
                case (int)ActionEnum.DashLeft:
                    Movement(Vector2.left);

                    break;
            }

            yield return null;
        }
    }
    
    private void Movement(Vector2 direction)
    {
        if (_dash)
        {
            _rigidbody.AddForce(direction * _force);
        }
        else
        {
            if (_addForce)
            {
                _rigidbody.AddForce(direction * _forceMove);
            }
            else
            {
                _rigidbody.MovePosition(_rigidbody.position + direction * _velocity.x * Time.deltaTime);
            }
        }
    }

    private void ResetLife()
    {
        Life = 3;
    }

    public void GetDamage()
    {
        Life--;

        if (OnLifeChange != null)
        {
            OnLifeChange.Invoke();
        }
    }
    
    public void GetLife()
    {
        if (Life < 3)
        {
            Life++;

            if (OnLifeChange != null)
            {
                OnLifeChange.Invoke();
            }
        }
    }
   
}
