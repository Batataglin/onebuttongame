﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SocialPlatforms.Impl;

public class Fall : MonoBehaviour
{
    private enum Class
    {
        Cat,
        Point,
        Damage,
        Dog
    }

    [SerializeField] private int _pointsEarn;
    [SerializeField] private int _pointsLoss;
    [SerializeField] private Vector2 _fallSpeed;
    [SerializeField] private Vector2 _catWalkSpeed;
    [SerializeField] private Class _class;

    private Rigidbody2D _rigidbody;
    private PlayerController _player;
    private GameManager _gameManager;
    private AudioSource _audio;
    
    private void OnEnable()
    {
        _player = PlayerController.Instance.GetComponent<PlayerController>();
        _gameManager = GameObject.FindWithTag("GameController").GetComponent<GameManager>();
        _rigidbody = GetComponent<Rigidbody2D>();
        _audio = GetComponent<AudioSource>();
        StartCoroutine(Fallen());
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var otherTag = other.gameObject.tag;
        
        if (otherTag == "Player")
        {
            switch (_class)
            {
                case Class.Cat:
                case Class.Point:
                    _gameManager.UpdateScore(_pointsEarn);
                    _audio.Play();
                    
                    break;
                case Class.Dog:
                    _player.GetLife();

                    break;
                case Class.Damage:
                   _player.GetDamage();

                    break;
            }

            Destroy(gameObject);
        }
        else if (otherTag == "Ground")
        {
            switch (_class)
            {
                case Class.Cat:
                    // Physics2D.IgnoreCollision(_player.GetComponent<Collider2D>(),gameObject.GetComponent<Collider2D>(),true);
                    // gameObject.GetComponent<Collider2D>().isTrigger = false;
                    // StartCoroutine(WalkAway());

                    Destroy(gameObject);
                    
                    break;
                case Class.Point:
                case Class.Dog:
                    _gameManager.UpdateScore(-_pointsLoss);
                    Destroy(gameObject);

                    break;
                
                case Class.Damage:

                    Destroy(gameObject);
                    break;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("SideCollider"))
        {
            StopCoroutine(WalkAway());
            Destroy(gameObject);
        }
    }
    
    private IEnumerator WalkAway()
    {
        var direction = Vector2.left;
        
        _rigidbody.simulated = false;
        _catWalkSpeed = new Vector2(2, 0);
        
        while (true)
        {
            _rigidbody.MovePosition(_rigidbody.position + direction * _catWalkSpeed.x * Time.deltaTime);

            yield return null;
        }
    }

    private IEnumerator Fallen()
    {
        while (true)
        {
            _rigidbody.MovePosition(_rigidbody.position + Vector2.down * _fallSpeed.y * Time.deltaTime);

            yield return null;
        }
    }
}
