﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class SpawnManager : MonoBehaviour
{
    [Header("Spawn Controller")]
    [SerializeField, Range(0f, 100f)] private float _adultSpawnRate;
    [SerializeField, Range(0f, 100f)] private float _kidSpawnRate;
    [SerializeField, Range(0f, 100f)] private float _oldSpawnRate;
    [SerializeField, Range(0f, 100f)] private float _catSpawnRate;
    [SerializeField, Range(0f, 100f)] private float _dogSpawnRate;
    [SerializeField, Range(0f, 100f)] private float _fireSpawnRate;
    [SerializeField, Range(0f, 100f)] private float _nothingSpawn;
    [SerializeField] private float _spawnRateTime;
    [SerializeField] private int _quantityOfSpawn;
    
    [Header("Prefab reference")]
    [SerializeField] private GameObject _fire;
    [SerializeField] private GameObject _adult;
    [SerializeField] private GameObject _kid;
    [SerializeField] private GameObject _old;
    [SerializeField] private GameObject _dog;
    [SerializeField] private GameObject _cat;

    [Space]
    [SerializeField] private Transform _spawnPoint;
    [SerializeField] private Transform _fallInstace;

    public static float SpawnRate { get; set; }    
    public static float SpawnQuantity { get; set; }

    private void OnEnable()
    {
        SpawnRate = _spawnRateTime;
        SpawnQuantity = _quantityOfSpawn;
        
        StartCoroutine(SpawnControl());
    }

    private void OnDisable()
    {
        StopCoroutine(SpawnControl());
    }

    private IEnumerator SpawnControl()
    {
        while (true)
        {
            SpawnObjects();

            yield return new WaitForSeconds(SpawnRate);
        }
    }

    private void SpawnObjects()
    {
        for (int i = 0; i < SpawnQuantity; i++)
        {
            var spawnPosition = _spawnPoint.position;

            spawnPosition.x = spawnPosition.x + Random.Range(0.0f, 4f);
            spawnPosition.y = spawnPosition.y + Random.Range(0.0f, 3f);

            var spawnedObject = RandomizeObject();                 
            
            if (spawnedObject != null)
            {
                var instance = Instantiate(spawnedObject,_fallInstace);
                instance.transform.position = spawnPosition;
            }
        }
    }

    private GameObject RandomizeObject()
    {
        GameObject objectToSpawm = null;
        var random = Random.Range(0f, 100f);

        // 30%
        if (random < 30f)
        {
            objectToSpawm = null;
        }

        // 25%
        else if (random >= 30f && random < 55f)
        {
            objectToSpawm = _fire;
        }

        // 15%
        else if (random >= 55f && random < 70f)
        {
            objectToSpawm = _old;
        }

        // 15%
        else if (random >= 70f && random < 85f)
        {
            objectToSpawm = _adult;
        }

        // 10%
        else if (random >= 85f && random < 90f)
        {
            objectToSpawm = _kid;
        }

        // 3%
        else if (random >= 90f && random < 93f)
        {
            objectToSpawm = _dog;
        }
        
        // 2%
        else if ( random >= 93f && random < 100f)
        {
            objectToSpawm = _cat;
        }

        return objectToSpawm;
    }
    
    
}
