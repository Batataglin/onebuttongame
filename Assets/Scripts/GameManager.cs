﻿using System;
using System.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum GameStateEnum
{
    Start,
    Pause,
    Ingame,
    GameOver
}

public class GameManager : MonoBehaviour
{
    private static GameObject _instance;
    
    public static Action OnTapButton;
    public static Action OnResetGame;
    
    [SerializeField] private float _tapTime;
    
    [Header("Main Menu")]
    [SerializeField] private GameObject _menuScreen;
    [SerializeField] private SpriteRenderer _mainImage;
    [SerializeField] private Text _actionText;

    [Header("InGame")]
    [SerializeField] private GameObject _gameElements;
    [SerializeField] private GameObject _pauseScreen;
    [SerializeField] private GameObject _gameOverScreen;
    [SerializeField] private Text _highestScoreText;
    [SerializeField] private Transform _fallInstace;
    
    [Header("Balancing")]
    [SerializeField] private float _spawnRate = 2f;
    [SerializeField] private float _spawnQuantity = 7f;

    private const string _playerPrefScore = "HighScore";
    
    private bool _startingGame;
    private int _highScore;
    private int _currentScore;
    private float _inputTimer;
    private float _timeScale;
    private float _maxTimeScale = 1.75f;
    private Text _scoreText;
    private RawImage[] _lifeSprite;
    private Vector2 _playerInitalPosition;  
    
    public static GameStateEnum GameState { get; set; }
    public GameObject Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = gameObject;
        }
        
         PlayerController.OnLifeChange += UpdateLife;
    }

    private void OnEnable()
    {
        _playerInitalPosition = new Vector2(-1.4f,-1.5f);
        _lifeSprite = null;
        _lifeSprite = new RawImage[3];
        _lifeSprite = GameObject.FindWithTag("Lifes").GetComponentsInChildren<RawImage>();

        _scoreText = null;
        _scoreText = GameObject.FindWithTag("Score").GetComponent<Text>();

        _gameElements.SetActive(false);
        _pauseScreen.SetActive(false);
        _gameOverScreen.SetActive(false);
    }

    private void OnDisable()
    {
        Destroy(gameObject);
    }

    private void Start()
    {
        GameState = GameStateEnum.Start;
        _startingGame = true;
        _timeScale = 1f;
        _currentScore = 0;

        if (PlayerPrefs.HasKey(_playerPrefScore))
        {
            _highScore = PlayerPrefs.GetInt(_playerPrefScore);
        }
        else
        {
            PlayerPrefs.SetInt(_playerPrefScore, 0);
        }

        _highestScoreText.text = "High score : " + _highScore.ToString();
        
        UpdateScore(0);
        UpdateLife();
        
        StartCoroutine(BlinkText());
    }

    private void LateUpdate()
    {
        GameFlow();
    }

    private void GameFlow()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            _inputTimer += Time.deltaTime;            
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            if (_inputTimer <= _tapTime) // TAP
            {
                if (GameState == GameStateEnum.Ingame)
                {
                    OnTapButton?.Invoke();
                }
                else if (GameState == GameStateEnum.Pause)
                {
                    ReturnGame();
                }
                else if(GameState == GameStateEnum.GameOver)
                {
                    Restart();
                }
            }
            else // HOLD
            {
                if (GameState == GameStateEnum.Ingame)
                {
                    PauseGame();
                }
                else if (GameState == GameStateEnum.Pause)
                {
                    QuitGame();
                }
                else if (GameState == GameStateEnum.GameOver)
                {
                    QuitGame();
                }
                else
                {
                    if (_startingGame)
                    {
                        GameState = GameStateEnum.Ingame;
                        _startingGame = false;
                        _gameElements.SetActive(true);
                        StartCoroutine(StartGame());
                    }
                }
            }
            
            _inputTimer = 0f;
        }

    }

    private void Restart()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(0);

        if (OnResetGame != null)
        {
            OnResetGame.Invoke();
        }
    }

    private void ReturnGame()
    {
        _pauseScreen.SetActive(false);
        Time.timeScale = _timeScale;
        GameState = GameStateEnum.Ingame;
    }

    private void PauseGame()
    {
        _pauseScreen.SetActive(true);
        Time.timeScale = 0f;
        GameState = GameStateEnum.Pause;
    }

    private void GameOver()
    {
        Time.timeScale = 0f;
        _gameElements.SetActive(false);
        _gameOverScreen.SetActive(true);
        SaveScore();
    }

    private void SaveScore()
    {

        if (_currentScore > _highScore)
        {
            _highScore = _currentScore;
            PlayerPrefs.SetInt(_playerPrefScore, _highScore);
        }
    }
    
    private void QuitGame()
    {
        SaveScore();
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#endif
        Application.Quit();
    }

    private IEnumerator BlinkText()
    {
        var timer = 0f;
        var timerIncrement = true;
        while (true)
        {
            if (timer <= 0f)
            {
                timerIncrement = true;
            }
            else if(timer >= 1f)
            {
                timerIncrement = false;
            }

            if (timerIncrement == true)
            {
                timer += Time.deltaTime;
            }
            else
            {
                timer -= Time.deltaTime;
            }
                
            _actionText.color = new Color(1,1,1,timer);
            
            yield return null;
        }
    }

    private IEnumerator StartGame()
    {
        var timer = 1f;
        var color = _mainImage.color; 
        
        StopCoroutine(BlinkText());
        _actionText.gameObject.SetActive(false);
        
        while (_mainImage.color.a >= 0f)
        {
            timer -= Time.deltaTime;

            _mainImage.color = new Color(color.r, color.g, color.b,timer);

            yield return null;
        }

        _menuScreen.SetActive(false);
    }

    public void UpdateScore(int score)
    {
        _currentScore += score;

        if (_currentScore < 0)
        {
            _currentScore = 0;
        }
        
        if (_currentScore % 1000 == 0)
        {
            if (_timeScale <= _maxTimeScale)
            {
                _timeScale += 0.05f;
                Time.timeScale = _timeScale;
            }

            if (SpawnManager.SpawnRate >= 3f)
            {
                SpawnManager.SpawnRate -= 0.5f;
            }

            if (SpawnManager.SpawnQuantity < 7f)
            {
                SpawnManager.SpawnQuantity += 0.1f;
            }
        }
        _scoreText.text = "Score: " + _currentScore;
    }

    public void UpdateLife()
    {
        var playerLife = PlayerController.Life;
        
        for (int i = 0; i < _lifeSprite.Length; i++)
        {
            _lifeSprite[i].enabled = false;
        }
        
        for (int i = 0; i < playerLife; i++)
        {
            _lifeSprite[i].enabled = true;
        }

        if (playerLife <= 0)
        {
            GameManager.GameState = GameStateEnum.GameOver;
            GameOver();
        }
    }
}
